from django.shortcuts import render, redirect
from .forms import TaskForm
from .models import Task


def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    return render(request, "tasks/create_task.html", {"form": form})


def show_my_tasks(request):
    user = request.user
    tasks = Task.objects.filter(assignee=user)

    return render(request, "tasks/my_task.html", {"tasks": tasks})
