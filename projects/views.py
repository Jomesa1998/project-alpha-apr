from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from .models import Project
from .forms import ProjectForm


@login_required
def list_projects(request):
    if request.user.is_authenticated:
        projects = Project.objects.filter(owner=request.user)
        context = {"projects": projects}
        return render(request, "projects/detail.html", context)
    else:
        return HttpResponseForbidden(
            "You must be logged in to access this page."
        )


def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    return render(
        request, "projects/projects_detail.html", {"project": project}
    )


def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    return render(request, "projects/create_project.html", {"form": form})
